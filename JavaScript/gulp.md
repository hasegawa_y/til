# gulp.task内で下記のようにreturnで返さないと実際のコンパイルにかかる時間がわかりづらくなる。

### ダメな例

```
gulp.task('uglify', function () {
    gulp.src(js_files)
        .pipe(concat('min.js'))
        .pipe(gulp.dest('js/build'))
        .pipe(uglify());
});
```

### OKな例

```
gulp.task('uglify', function () {
    return gulp.src(js_files)
        .pipe(concat('min.js'))
        .pipe(gulp.dest('js/build'))
        .pipe(uglify());
});
```

